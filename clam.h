/* clam header thingy */
/* Clam version 0.1 - copyright carey pridgeon <c.pridgeon@ex.ac.uk> 2005
 * 		  Released under GNU GENERAL PUBLIC LICENSE
		       Version 2, June 1991 -  or later
*/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>

#define sqr(x) ((x)*(x))
#define REALLY_BIG_NUMBER 1000000000
int **load_graph (char *filename, int col, int *size);


	
