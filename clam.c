#include "clam.h"
/* Clam version 0.1 - copyright carey pridgeon <c.pridgeon@ex.ac.uk> 2005
 * 		  Released under GNU GENERAL PUBLIC LICENSE
		       Version 2, June 1991 -  or later
*/
int main (int argc,char **argv) {
    int a,b,c,check1,check2,index,local_max_temp,local_max_current,local_max_index;
    int local_min_temp,local_min_current,local_min_index,max_count,min_count;
    int column_number, relevent_column,hist_size,check,*column_length,temp_max,temp_min;
    int **graph,*minima,*maxima,*maxima_final,*minima_final,*maxima_index_final,*minima_index_final;
    int counter1,counter2;
    int average_1,average_2;
    int lowest_max,highest_max,lowest_min,highest_min;
    float maxima_squared_dev,minima_squared_dev,max_std_dev,min_std_dev;
    int lowest_max_index_distance,highest_max_index_distance,lowest_min_index_distance,highest_min_index_distance;
    float maxima_index_squared_dev,minima_index_squared_dev,max_index_std_dev,min_index_std_dev;    
    if (argc==1) {
         printf("\n> Clam version 0.1 - copyright carey pridgeon <c.pridgeon@ex.ac.uk> 2005\n>\n");   
	 printf("> Clam requires four command line options\n");
	 printf("> Example:\n");	    
	 printf(">          ./clam 2 2 10 datafile.txt\n");
	 printf("> perameter one is how many columns there are in the data file.\n");	 
	 printf("> perameter two refers to the column you want to analyse.\n");
	 printf("> perameter three is the the error margin.\n");	 
	 printf(">   This is a value that will be used when making sure\n");	 	 
	 printf(">   that a suspected local maxima or minima is indeed that,\n");	 
	 printf(">   and not a small fluctuation in the data that would\n");	        
	 printf(">   produce false results\n");  
	 printf("> perameter four is the filename to be analysed.\n");
	 printf(">   This needn't have a txt extension, but it does need\n");		 
	 printf(">   to be a plain text file with the numbers in columns.\n");	
	 exit(0);
    }    
    if (argc!=5) {
	    printf("> Error, incorrect number of command line options\n");
	    exit(0);
    }

    /* the values passed at the command line are strings, so
     * these need to become ints, except for the filename
     */
    column_number = atoi(*(argv + 1));
    relevent_column = atoi(*(argv + 2));
    hist_size = atoi(*(argv + 3));
    
    /* malloc the var used to store the length of the columns
     */     
    column_length = malloc(sizeof(int));
    
    /* load the file that contains the graph data
     * and get the length of the columns as well
     */
    graph = load_graph (*(argv + 4),column_number,column_length);

    /* the column length is known now, so 
     * the results arrays can now be created */
	    
    minima = calloc((size_t)*column_length,sizeof(int));
    maxima = calloc((size_t)*column_length,sizeof(int));

  

   /* get all the local maxima */
   check= check1= check2 =0; 
   temp_max = temp_min = 0;
   for(a=0;a<*column_length;a++) {
       maxima[a] = -1;
       minima[a] = -1;
   }
   index=hist_size;
   printf("\n> Datafile detail:\n");
   printf("> filename              : %s\n",*(argv + 4));   
   printf("> number of columns     : %d\n",column_number);
   printf("> column size           : %d elements\n",*column_length);
   printf("> column being examined : %d\n",relevent_column);
    max_count = min_count = 0;
    while(1) {
                check= check1= check2 =0; 	 	    
                while(check1==0) {/* find the region containing the local maxima */
	              if((graph[relevent_column-1][index-hist_size] < graph[relevent_column-1][index])&&(graph[relevent_column-1][index+hist_size] < graph[relevent_column-1][index])) {	
                       check1=1; /* found one */
	         }
	       index++;	
	   }
		/* got the region, so localise it */
                local_max_temp = local_max_current = local_max_index = 0;
                for(a=index-hist_size;a<index+hist_size;a++) {
                    local_max_temp = graph[relevent_column-1][a];
                    if(local_max_temp > local_max_current) {
		       local_max_current=local_max_temp;	    
		       local_max_index = a; 
		    }
                }
		/* store this single result and jump forward a bit to
		 * avoid registering this result multiple times
		 */
                maxima[local_max_index] = graph[relevent_column-1][local_max_index];
                max_count++;
	        index+=hist_size;
                check++;
	        /* exit if at end of column */
  	        if(index>*column_length) {
                   break;
	        }
		
                while(check2==0) {/* find the region containing the local minima */
	              if((graph[relevent_column-1][index-hist_size] > graph[relevent_column-1][index])&&(graph[relevent_column-1][index+hist_size] > graph[relevent_column-1][index])) {	
                         check2=1; /* found one */
	              }
	             index++;	
	        /* exit if at end of column */
  	        if(index>*column_length) {
                   break;
	        }}
		/* got the region, so localise it */
                local_min_temp = local_min_current = maxima[local_max_index];
		local_min_index = 0;
                for(a=index-hist_size;a<index+hist_size;a++) {
                    local_min_temp = graph[relevent_column-1][a];
                    if(local_min_temp < local_min_current) {
		       local_min_current=local_min_temp;	    
		       local_min_index = a; 
		    }
                }
		/* store this single result and jump forward a bit to
		 * avoid registering this result multiple times
		 */		
                minima[local_min_index] = graph[relevent_column-1][local_min_index];
                min_count++;
	        index+=hist_size;
                check++;
	        /* exit if at end of column */
  	        if(index>*column_length) {
                   break;
	        }
    }
    /* got the required datapoints, so create the arrays that will be used
     * to manipulate the data
     */
    maxima_final = calloc((size_t)max_count-1,sizeof(int));
    minima_final = calloc((size_t)min_count-1,sizeof(int));
    maxima_index_final = calloc((size_t)max_count-1,sizeof(int));
    minima_index_final = calloc((size_t)max_count-1,sizeof(int));
    counter1 = counter2 = 0;
    
    /*extract the maxima and minima into new arrays for analysis */
    for(b=0;b<*column_length;b++) {
        if(maxima[b]!=-1) {
           maxima_final[counter1] = maxima[b];
	   maxima_index_final[counter1] = b;
           counter1++;	   
	}
	if(counter1==max_count-1) {
             break;
	}
    }
    for(c=0;c<*column_length;c++) {
        if(minima[c]!=-1) {
           minima_final[counter2] = minima[c];
	   minima_index_final[counter2] = c;
           counter2++;	   
	}
	if(counter2==min_count-1) {
             break;
	}
    }

    /* analysis and display of results bit */
    
    printf("\n|----------------------\n");
    printf("|- Local Maxima results\n");
    printf("|----------------------\n\n");
    
    printf("Number of Local Maxima found: %d\n",min_count-1); 
    printf("All local Maxima found\n"); 
    printf("\tindex\t value:\n");    
    for(b=0;b<max_count-1;b++) {
        printf("\t %d\t %d\n",maxima_index_final[b],maxima_final[b]);  
    } 
    /*average local maxima height */
    average_1 = average_2 = 0;
    for(b=0;b<max_count-1;b++) {
        average_1+=maxima_final[b];
    }    
    average_1 = average_1/(max_count-1);


    /* get the range of values in local maxima */
    /* lowest local maxima value*/
    lowest_max = maxima_final[0];
    for(b=1;b<max_count-1;b++) {
   	if(maxima_final[b]<lowest_max) {
           lowest_max = maxima_final[b];
	}
    }  
    highest_max = maxima_final[0];
    /* highest local maxima value*/
    for(b=1;b<max_count-1;b++) {
        if(maxima_final[b]>lowest_max) {
           highest_max = maxima_final[b];
	}	     
    }      

    
    maxima_squared_dev = 0.0;   
    /* get the square of all deviations from the average */
    for(b=1;b<max_count-1;b++) {
        maxima_squared_dev += sqr(maxima_final[b] - average_1);
    } 
    /* getting the sqrt, but using n-1, rather then n, as this is a small sample*/
    max_std_dev = sqrt((maxima_squared_dev)/((max_count-1)-1));
    printf("\nAverage Local Maxima Height       : %d\n",average_1);
    printf("Local Maxima Range                : %d\n",highest_max-lowest_max);    
    printf("Local Maxima Standard Deviation   : %2.2f\n\n",max_std_dev);

    /* now for some analysis of local maxima indexes*/
    
    /*average distance between local maxima indexes found*/
    average_1 =0;
    for(b=1;b<max_count-1;b++) {
        average_1+=maxima_index_final[b] - maxima_index_final[b-1];
    }    
    average_1 = average_1/(max_count-2);




    /* get the range of values in local maxima indexes*/
    /* lowest local maxima value*/
    lowest_max_index_distance = REALLY_BIG_NUMBER;
    for(b=1;b<max_count-1;b++) {
   	if((maxima_index_final[b] - maxima_index_final[b-1])<lowest_max_index_distance) {
           lowest_max_index_distance = (maxima_index_final[b] - maxima_index_final[b-1]);
	}
    } 

    /* highest local maxima value*/
    highest_max_index_distance = 0;
    for(b=1;b<max_count-1;b++) {
   	if((maxima_index_final[b] - maxima_index_final[b-1])>highest_max_index_distance) {
           highest_max_index_distance = (maxima_index_final[b] - maxima_index_final[b-1]);
	}
    }  


    maxima_index_squared_dev = 0.0;   
    /* get the square of all deviations from the average */
    for(b=1;b<max_count-1;b++) {
        maxima_index_squared_dev += sqr((maxima_index_final[b] - maxima_index_final[b-1]) - average_1);
    } 

    /* getting the sqrt, but using n-1, rather then n, as this is a small sample*/
    max_index_std_dev = sqrt((maxima_index_squared_dev)/((max_count-2)-1));

   printf("\nAverage Local Maxima Frequency              : %d\n",average_1);
   printf("Local Maxima Frequency Range                : %d\n",highest_max_index_distance-lowest_max_index_distance);       
   printf("Local Maxima Frequency Standard Deviation   : %2.2f\n\n",max_index_std_dev);
    

    printf("\n|----------------------\n");
    printf("|- Local Minima results\n");
    printf("|----------------------\n\n");     
    printf("Number of Local Minima found: %d\n\n",min_count-1);   
    printf("All Local Minima found\n"); 
    printf("\tindex\t value:\n");    
    for(b=0;b<min_count-1;b++) {
        printf("\t %d\t %d\n",minima_index_final[b],minima_final[b]);  
    }      
    /*average local minima height */
    for(b=1;b<min_count-1;b++) {
        average_2+=minima_final[b];
    }    
    average_2 = average_2/(min_count-1); 

     /* get the range of values in local maxima */   
     /* lowest local minima value*/
     lowest_min = minima_final[0];    
     for(b=1;b<min_count-1;b++) {
	 if(minima_final[b]<lowest_min) {
            lowest_min = minima_final[b];
	 }	     
     }        
     /* highest local minima value*/
     highest_min = minima_final[0];     
     for(b=1;b<min_count-1;b++) {
	 if(minima_final[b]>lowest_min) {
            highest_min = minima_final[b];
	 }		     
     }      

    
    /* now get the standard deviation */   
    minima_squared_dev = 0.0;   
    /* get the square of all deviations from the average */
    for(b=1;b<min_count-1;b++) {
        minima_squared_dev += sqr(minima_final[b] - average_2);
    } 
    /* getting the sqrt, but using n-1, rather then n, as this is a small sample*/
    min_std_dev = sqrt((minima_squared_dev)/((min_count-1)-1));

    printf("\nAverage Local Minima Height       : %d\n",average_2); 
    printf("Local Minima Range                : %d\n",highest_min-lowest_min); 
    printf("Local Minima Standard Deviation   : %2.2f\n\n",min_std_dev);


    
    /* now for some analysis of local minima indexes*/
    
    /*average distance between local minima indexes found*/
    average_2 =0;
    for(b=1;b<min_count-1;b++) {
        average_2+=minima_index_final[b] - minima_index_final[b-1];
    }    
    average_2 = average_2/(min_count-2);




    /* get the range of values in local minima indexes*/
    /* lowest local minima value*/
    lowest_min_index_distance = REALLY_BIG_NUMBER;
    for(b=1;b<min_count-1;b++) {
   	if((minima_index_final[b] - minima_index_final[b-1])<lowest_min_index_distance) {
           lowest_min_index_distance = (minima_index_final[b] - minima_index_final[b-1]);
	}
    } 

    /* highest local minima value*/
    highest_min_index_distance = 0;
    for(b=1;b<min_count-1;b++) {
   	if((minima_index_final[b] - minima_index_final[b-1])>highest_min_index_distance) {
           highest_min_index_distance = (minima_index_final[b] - minima_index_final[b-1]);
	}
    }  
  

    minima_index_squared_dev = 0.0;   
    /* get the square of all deviations from the average */
    for(b=1;b<min_count-1;b++) {
        minima_index_squared_dev += sqr((minima_index_final[b] - minima_index_final[b-1]) - average_2);
    } 

    /* getting the sqrt, but using n-1, rather then n, as this is a small sample*/
    min_index_std_dev = sqrt((minima_index_squared_dev)/((min_count-2)-1));
   printf("\nAverage Local Minima Frequency              : %d\n",average_2);
   printf("Local Minima Frequency Range                : %d\n",highest_min_index_distance-lowest_min_index_distance); 
   printf("Local Minima Frequency Standard Deviation   : %2.2f\n\n",min_index_std_dev);
   

  free(minima);
  free(maxima);
  free(column_length); 
  free(maxima_final); 
  free(maxima_index_final); 
  free(minima_final); 
  free(minima_index_final);  
  for (c=0;c<column_number;c++) {
      free(graph[c]);
  }
  free(graph);
   
    return 0;

  }

