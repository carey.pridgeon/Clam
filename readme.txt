Clam version 0.1 - carey pridgeon 2005
Released under GNU GENERAL PUBLIC LICENSE Version 3

Clam is a simple utility that can locate all the local maxima and minima 
in a column of numbers.

It also works out a few additional things, such as average height of maxima 
and minima, standard deviation and average distance between their occurance.

Usage:
	  Clam requires four command line options
	  Example: 
	           ./clam 2 2 10 datafile.txt;

	  perameter one is how many columns there are in the data file.	 

	  perameter two refers to the column you want to analyse.

	  perameter three is the the error margin. 
	    This is a value that will be used when making sure	 	 
	    that a suspected local maxima or minima is indeed that;	 
	    and not a small fluctuation in the data that would	        
	    produce false results  

	  perameter four is the filename to be analysed.
	    This needn't have a txt extension, but it does need		 
	    to be a plain text file with the numbers in columns.
