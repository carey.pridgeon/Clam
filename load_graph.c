#include "clam.h"
/* Clam version 0.1 - copyright carey pridgeon <c.pridgeon@ex.ac.uk> 2005
 * 		  Released under GNU GENERAL PUBLIC LICENSE
		       Version 2, June 1991 -  or later
*/
int **load_graph (char *filename, int col, int *size) {
int a,c,b;
int sample_num,pos,wpos;
size_t s_num;

FILE *inputfile;
char *word;
char *temp_string,*inputline;
int **storage;



      /* check the size of the file in lines */

      temp_string = malloc(1024*sizeof(char));
      word = malloc(32*sizeof(char));      
      inputline = malloc(1024*sizeof(char)); 
      inputfile = fopen(filename,"r");
      if(inputfile == NULL){
         fprintf (stderr,"> B0rk3d on file load for %s\n",filename);
	 exit(0);
      }

     /* reset the file pointer to start of file */
     if ((fseek(inputfile,0,SEEK_SET)) ==-1) {
     	printf ("> Error on file pointer reset for %s",filename);
     	
     }

     /* how many lines are in this file?*/
     sample_num = 0;
     while((fgets(temp_string,200, inputfile)) != NULL ){
           sample_num++;
     }
     s_num = (size_t) sample_num;
     *size = sample_num; /* used to tell the calling function
			    how big the 2d array created is
			  */
     
     /* calloc the array that will hold the columnated data.
      */
     storage = (int **) calloc((size_t)col,sizeof(int*));
     for (c=0;c<col;c++) {
          storage[c] =(int *) calloc(s_num,sizeof(int));
     }

     /* reset the file pointer to start of file */
     if ((fseek(inputfile,0,SEEK_SET)) ==-1) {
     	printf ("> Error on file pointer reset for %s",filename);
     }
     
     for (c=0;c<32;c++) { /* blank the buffer used to collect the number*/
           word[c] =' ';
     }
     /* extract the actual numbers, and stuff*/
     
     for(a=0;a<sample_num;a++) {/* go down the file, a line at a time*/
         fgets(inputline,100, inputfile); /* fetch the line */
         pos = 0;
         wpos=0; 
         if (inputline[pos] =='\0') { break;} /* copes with newline at end of file */
	 for(b=0;b<col;b++) {/* extract each colunm entry using fancy parsing stuff */
             while ((inputline[pos] == ' ')||(inputline[pos]=='\t')) {/* move between numbers */
                    pos++;		
              }	
              wpos = 0;	     
              while ((inputline[pos] !='\0') && (inputline[pos]!=' ')&&(inputline[pos]!='\n')&&(inputline[pos]!='\t')) {
                    word[wpos] = inputline[pos];
                    wpos++;
                    pos++;		
              }
	      storage[b][a] = atof(word); /* store the value extracted in the array for that column*/     
	      for (c=0;c<32;c++) { /* blank the buffer used to collect the number again, ready for re-use*/
                  word[c] =' ';
	      }
	 }		
     }

     /*close the input file */
     fclose(inputfile);
     /* free the memory used here */
     free(word);     	 
     free(temp_string);
     /* pass back a pointer the the fresh new 2d array of 
      * integer yumminess
     */
     return storage;
}




     
