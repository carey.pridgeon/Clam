#################################################
# Makefile for clam                             #
#################################################

CC = gcc
CFLAGS_DB = -g
CFLAGS = -Wall -O3 
LLIBS =  -lm
EXE = clam

OBJS = $(patsubst %.c,%.o,$(wildcard *.c))

#----------
clam: $(OBJS)
	$(CC) $(LLIBS) $(OBJS)  -o $(EXE)


#----------

$(OBJS): %.o : %.c
	$(CC) $(CFLAGS) -c $<
	
#----------

clean:
	rm -f *.o *.*~ *~ core $(EXE)

#----------

